//
//  ViewController.swift
//  ReadingMeter
//
//  Created by SHOKI TAKEDA on 11/9/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class JapaneseViewController: UIViewController, NADViewDelegate {
    private var nadView: NADView!
    var adTimer:NSTimer!
    var timer:NSTimer!
    var counter:Float = 0
    var countTextNumbers = 0
    var totalReadingNumbers = 0
    func wordCount(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    @IBAction func showTextAction(sender: UIButton) {
        let random = arc4random_uniform(5)
        textLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        switch random {
        case 1:
            textLabel.text = "日本の科学教育は科学技術人材の育成と国民の科学的リテラシーの向上という2つの課題を対処するため に，子供たちに創造的活動の基盤を身に付けさせ専門性や個性を発展できる学習機会を拡大している1)。文部科学省や教育委員会も国際競争が激化する現代社会において，新たな学問分野や急速な技術革新に対応できる深い専門知識と幅広い応用力を持つ人材を育成しようとテスト評価や方策の実施を試みている2)。興味深い取り組みとして，「中学生・高校生の科学・技術研究論文　野口英世賞」をはじめ科学技術に関する論文を書く機会が設けられているが，参加者が限定されていることや指導者なしでの論文の執筆が難しいことが問題点であり，国民全体の科学リテラシーを上昇させることは難しいと考えられる。科学技術人材の育成と国民の科学的リテラシーの向上のためには生涯学習を重視し，誰もが気軽にアクセスでき，興味を持ちながら持続的に学習することが必要である。"
        case 2:
            textLabel.text = "みなさんは「巨人の肩の上に立つ」という言葉をご存じだろうか。Google Scholarという学術論文を閲覧するサービスのトップページで私はこの言葉をよく目にする。私以外にも研究活動などに従事し文献を検索した経験を持つ方はご存知なのではないであろうか。ニュートンは1676年にフックとの裁判中に彼に宛てた手紙においてこの言葉を引用し，自分の業績や現在ある科学技術は過去の偉人達の残した業績をもとにできているということを主張した。現代における新たな発見や私たちが普段何気なく使っている製品についても，すべてが新しいものはないと言っても過言ではないと言える。学術論文には，科学技術のプロセスや業績が詳細に記述されており，学術論文は現代科学に関する成果の集合体である。まさに論文を読むことは現代科学を理解するうえで必要不可欠なことであると言える。"
        case 3:
            textLabel.text = "私が研究をしていてやりがいや楽しさを感じるのは，自分なりに工夫して結果を出すという自己の考えを具現化するところである。それは，他者の学術論文を読んでいても感じることで，論文に記述されているアプローチの工夫や新たな発見によって科学の楽しさを感じることができる。論文を読むことによってアプローチの工夫，新しい発見を疑似体験して実践的な科学の楽しさを感じることができれば，中高学生の理科離れを止める一助になるのではないだろうか。したがって，学校推薦論文によって論文を読むきっかけをつくることによって子どもから大人まで自分の関心があるテーマに関して自ずと論文をデータベースで検索して自学する環境を作り上げる。そのためには，学習の習慣を形成するうえで重要な時期である初等，中等教育において経験して興味を持つことが後の継続的な学習を支えるうえで欠かせない。"
        case 4:
            textLabel.text = "私が提案するこの学校推薦論文は文部科学省，独立行政法人科学技術振興機構，国立教育政策研究所など科学技術人材の育成と国民の科学的リテラシーの向上の責任を担う公的機関が導入するべきと考える。そして中高生が理解できるレベルの最新の科学技術に関連する論文を選出し推薦する。可能な限りGoogle Scholarのような誰でもアクセスできるフリーの学術論文データベースに掲載されているものを推薦することが望まれる。また，中高生が統計や専門用語の知識なしに学術論文を読むことは容易ではないため学習支援を学校が中心となって行う必要がある。論文の執筆は誰もができることではないが，無料で高品質の知的コンテンツに誰もがアクセスできるように近年なってきたので学校が学習支援することで誰もが自主的に論文を読めるようになる。"
        default:
            textLabel.text = "この学校推薦論文は主に無料でアクセスできる論文を対象とするべきと考えているので，メリットとしてはコストが低いことがあげられる。Google ScholarやCiNii Articlesをはじめ多くの論文データベースでは無料でアクセスできる論文が多く掲載されているので非常に多くの人が気軽に情報資源にアクセスできる。また，学術論文は理論や知識が充実していることに加え，その理論などを実際の問題点などに応用し，改善，解決するまでのプロセスが詳細に書かれている。したがって，実地的なレベルでの科学技術に触れることができるのである。これは，文部科学省やOECDが求めている科学的リテラシーそのものであり，日本人に欠けている科学的リテラシーの一部である論理的な思考力の向上にも貢献するため実効性が十分にあると言える。 "
        }
        
        countTextNumbers = textLabel.text!.characters.count
        startReadingBtn.hidden = false
        countLabel.hidden = true
        finishCountBtn.hidden = true
    }
    @IBAction func startCountAction(sender: UIButton) {
        finishCountBtn.hidden = false
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startReadingBtn.hidden = true
    }
    func onUpdate(){
        counter += 1
    }
    @IBAction func stopCountAction(sender: UIButton) {
        countLabel.hidden = false
        timer.invalidate()
        totalReadingNumbers = Int(600 / counter) * countTextNumbers
        countLabel.text = String(totalReadingNumbers) + " 文字/分"
    }
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var finishCountBtn: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var startReadingBtn: UIButton!
    @IBOutlet weak var showText: UIButton!
    
    override func viewDidLoad() {
        startReadingBtn.hidden = true
        finishCountBtn.hidden = true
        countLabel.hidden = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "01.jpg")!)
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        // NADViewクラスを生成
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        // 広告枠のapikey/spotidを設定(必須)
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        
        // 通知有無にかかわらずViewに乗せる場合
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}