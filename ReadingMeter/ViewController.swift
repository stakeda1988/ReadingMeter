//
//  ViewController.swift
//  ReadingMeter
//
//  Created by SHOKI TAKEDA on 11/9/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NADViewDelegate {
    @IBOutlet weak var finishBottom: NSLayoutConstraint!
    
    @IBOutlet weak var mainRight: NSLayoutConstraint!
    @IBOutlet weak var recordBottom: NSLayoutConstraint!
    @IBOutlet weak var finishTop: NSLayoutConstraint!
    @IBOutlet weak var mainLeft: NSLayoutConstraint!
    @IBOutlet weak var topTextHeight: NSLayoutConstraint!
    @IBOutlet weak var topTextBottom: NSLayoutConstraint!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    private var nadView: NADView!
    var adTimer:NSTimer!
    var timer:NSTimer!
    var counter:Float = 0
    var countTextNumbers = 0
    var totalReadingNumbers = 0
    func wordCount(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    @IBAction func showTextAction(sender: UIButton) {
        let random = arc4random_uniform(5)
        textLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        print(myBoundSize.height)
        
        if myBoundSize.height == 1024 || myBoundSize.height == 2048 {
            topTextHeight.constant = 80
            topTextBottom.constant = 80
            mainRight.constant = 50
            mainLeft.constant = 50
            recordBottom.constant = 220
            finishTop.constant = 50
            finishBottom.constant = 50
        } else if myBoundSize.height == 1366 {
            topTextHeight.constant = 150
            topTextBottom.constant = 150
            mainRight.constant = 150
            mainLeft.constant = 150
            recordBottom.constant = 440
            finishTop.constant = 100
            finishBottom.constant = 100
        }
        
        switch random {
        case 1:
            textLabel.text = " Starting next month, we are going to begin phasing out our bimonthly catalog and move everything online. Given the current rise in paper costs coupled with the increase in shipping costs, it is no longer financially feasible to send out a catalog to our regular customers, nor to those who request one. Based on feedback from customers, we've learned that most of them prefer placing orders online because it is convenient and saves them time. Our plan is to put our entire catalog online along with a few special features that can't be included in our print version due to cost, space, etc. First, we will have more pictures for each product. Second, there will be able to download all instruction manuals at no cost through our online store. Whether or not to provide a print catalog for a small fee is still being debated. We will have to see if there is a demand before we make a decision on that. I'll be sending up follow-up e-mails regarding the development of this project in the upcoming weeks."
        case 2:
            textLabel.text = " For the past 20 years, Satoh Goods has been the leading supplier of Japanese-made furniture and linens in the tai-state area. On behalf of everyone here at Kurata Goods, we wish to thank you for your continued patronage. Our store is celebrating its 20th Anniversary this month by offering our regular customers special discounts on our entire inventory. Choose from specially imported American beds and tables, leather French sofas and chairs, hight-quality Italian lamps, and much, much more! Customers who purchase an item from our antiques section will receive 20% off any additional piece of equal price or less. Sale lasts until the end of December, so don’t miss out. Customers who bring this postcard with them can use it to enter our drawing to win a Japanese Table with Mirror - a $4,300 value!"
        case 3:
            textLabel.text = " I know you’ve been quite busy with the Suzuki account, and with some of the recent staff shortages I realise your team has been working beyond its limits. I’m pleased to inform you that I’ll be transferring Yuta Satoh and Suzuki Taro to your team for the duration of your project. They just finished working on the Tanaka account the day before yesterday, so I’ll probably send them over either this Thursday or on Friday the 19th. They both have extensive experience in marketing ranging from small, private businesses to the large multinationals./n Another reason why I can send you Satoh and Suzuki is because on June 30 one summer intern and two new hires will begin working here. Once they finish up with general training, I’ll make a decision about where to send them. Since your team has been dealing with staff shortages, I was planing on sending one of the new hires and the intern to you. However, if you feel that you absolutely have no time to train them and that doing so would impede your ability to meet deadlines, I will send them to other teams. Please let me know before the end of this month."
        case 4:
            textLabel.text = " As of February 10, all customers who purchased XAIO laptops(models XAIO-54S and XAIO-35C) are asked to exchange their purchases as soon as possible. XAIO has issued a recall order for these models owing to faulty hard drives. These hard drives have shown signs of overheating and a tendency to lose data. Recent user complaints have led XAIO to issue a recall for all laptops in the 54S and 35C model series. All consumers who have purchased XAIO laptops that are NOT the XAIO-54S or XAIO-35C models may have defective batteries that overheat. If you have a laptop with a XAIO laptop battery XAC-40V, please bring in your battery and exchange it for a replacement. You may choose from a Xenix-brand battery or a Xenix-approved third-party battery (Razor, GHUN, or Crimson batteries are all approved). Choosing the latter option will not void your laptop’s warranty unless you choose a non-approved third-party product. For more information, please go to www.xaio.com/products."
        default:
            textLabel.text = " I know you’ve been busy dealing with the public relations side of this recall, but I have some important updates for you. The problems with the hard drives’ overheating have been confirmed to only occur in the XAIO-54S model. So far, any data loss on the XAIO-35C model has been due to user errors. As such, XAIO will only replace laptops for customers who bought the XAIO-54S. Customers who purchased the XAIO-35C may have their hard drive replaced for free, but we will not replace the entire laptop itself. As for the batteries, I would like you to update the list of approved third-party manufacturers. Please add Radio Time to the list and remove Razor from it. It turns out that Razor’s batteries are not completely compatible with our brand of laptops. Also, we need to keep stressing that the problem with the XDA-150V battery is not overheating, as initially reported, but its inability to hold a charge for more than one hour. I’ve already had a meeting with Hana Nakata, and he will take care of alerting retailers. I would appreciate it if you could update the FAQs section on the web so that it reflects these updates."
        }
        
        countTextNumbers = wordCount(textLabel.text!)
        startReadingBtn.hidden = false
        countLabel.hidden = true
        finishCountBtn.hidden = true
    }
    @IBAction func startCountAction(sender: UIButton) {
        finishCountBtn.hidden = false
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startReadingBtn.hidden = true
    }
    func onUpdate(){
        counter += 1
    }
    @IBAction func stopCountAction(sender: UIButton) {
        countLabel.hidden = false
        timer.invalidate()
        totalReadingNumbers = Int(600 / counter) * countTextNumbers
        countLabel.text = String(totalReadingNumbers) + " words/min"
    }
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var finishCountBtn: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var startReadingBtn: UIButton!
    @IBOutlet weak var showText: UIButton!
    
    override func viewDidLoad() {
        startReadingBtn.hidden = true
        finishCountBtn.hidden = true
        countLabel.hidden = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "01.jpg")!)
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        // NADViewクラスを生成
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        // 広告枠のapikey/spotidを設定(必須)
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        
        // 通知有無にかかわらずViewに乗せる場合
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}